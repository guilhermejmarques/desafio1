package br.com.itau;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {

	public static void main(String[] args) {
		double totalFatura = 0;

		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
		Collections.sort(lancamentos); //organizando lançametos por meses.

		//Apresetando cada laçamento linha a linha odenado por meses.
		for(Lancamento l : lancamentos){
				System.out.println(l);
		}

		//Lancamento de uma categoria escolhida.
		int categoria = 1;
		for(Lancamento l : lancamentos){
			if (l.getMes() == categoria){
				System.out.println("\n" + l);
			}
		}
		//Total da fatura de um mês escolhido.
		int mes = 7;
		for(Lancamento l : lancamentos){
			if (l.getMes() == mes){
				totalFatura = totalFatura + l.getValor();

			}
		}

		System.out.println("\nValor:" + totalFatura);
	}

}
